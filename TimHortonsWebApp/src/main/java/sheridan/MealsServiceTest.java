package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testDrinksRegular() {
		assertNotNull(MealType.DRINKS);
		
	}

	@Test
	public void testDrinksException() {
		assertFalse("No Brand Available", MealType.DRINKS == null);
		
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		assertTrue("Valid value for test", MealType.DRINKS != null);
	
	}
	@Test
	public void testDrinksBoundaryOut() {
		assertFalse("Invalid value for palindrome", MealType.BAKEDGOODS == null);
	
	}
	
	
	
	
	
	

}
